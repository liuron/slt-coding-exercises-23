#!/bin/bash

if [ ! -e ~/.ssh/slt2023_prv.key ]
then
    echo "> Private key ~/.ssh/slt2023_prv.key does not exist."
    exit 1
fi

notebookfile=$1

if [ ! -e "$notebookfile" ]
then
    echo "> Notebook file $notebookfile does not exist."
else
    openssl rsautl -decrypt -inkey ~/.ssh/slt2023_prv.key -in "$notebookfile" > "${notebookfile%'.encr'}"
    
    echo "> File decrypted as ${notebookfile%'.encr'}"
fi
