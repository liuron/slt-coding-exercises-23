#!/bin/bash

if [ ! -e slt2023_pub.key ]
then
    echo "> Public key ./slt2023_pub.key does not exist."
    exit 1
fi

notebookfile=$1

if [ ! -e "$notebookfile" ]
then
    echo "> Notebook file $notebookfile does not exist."
else
    openssl rsautl -encrypt -inkey slt2023_pub.key -pubin -in "$notebookfile" -out "$notebookfile.encr"
    
    echo "> File encrypted as $notebookfile.encr"
fi
